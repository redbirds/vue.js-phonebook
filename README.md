A books marketplace was written with Laravel 5.5 framework.

Functions:
- Authorization;
- User Roles:
    - Seller. Ability to add, edit, remove own books;
    - Administrator. Ability to add, edit, remove all books and sellers.
- Payments via Stripe;
